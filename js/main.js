
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.where1').on('click', function () {
        var check = true;

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            } else {
                saveEmail($)
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function validate(input) {
        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }



    /*==================================================================
     [ Simple slide100 ]*/

    $('.simpleslide100').each(function () {
        var delay = 7000;
        var speed = 1000;
        var itemSlide = $(this).find('.simpleslide100-item');
        var nowSlide = 0;

        $(itemSlide).hide();
        $(itemSlide[nowSlide]).show();
        nowSlide++;
        if (nowSlide >= itemSlide.length) { nowSlide = 0; }

        setInterval(function () {
            $(itemSlide).fadeOut(speed);
            $(itemSlide[nowSlide]).fadeIn(speed);
            nowSlide++;
            if (nowSlide >= itemSlide.length) { nowSlide = 0; }
        }, delay);
    });


})(jQuery);


const saveEmail = ($) => {
    const subscriptionUrl = 'https://pocketloan-api.herokuapp.com/v1/users/subscription'
    let authToken = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwb2NrZXRsb2FuLWNsaWVudCIsInN1YlR5cGUiOiJzZXJ2aWNlIiwiYXVkIjoicG9ja2V0bG9hbi5jbGllbnQuaW50ZXJuYWwiLCJpc3MiOiJwb2NrZXRsb2FuLmFwaS5pbnRlcm5hbCIsImlhdCI6MTUzNDcyMDY0OSwiZXhwIjoxNTM0NzI0MjQ5fQ.d3lSM4m-HNESBpAqIY2PlklJGRqfgH-myMiWZp0bmWM`

    const email = $(`.input100`).val()
    axios.defaults.headers.common['Authorization'] = authToken
    axios.post(subscriptionUrl, { email }).then(res => {
        $(`.input100`).val('')
        res.data
    }).catch(err => {
        throw err
    })
}

