
var countDownDate = new Date("Dec 20, 2018 00:00:00").getTime();
var monthsSpan = document.querySelector('.months');
var daysSpan = document.querySelector('.days');
var hoursSpan = document.querySelector('.hours');
var minutesSpan = document.querySelector('.minutes');
var secondsSpan = document.querySelector('.seconds');

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var months = Math.floor(distance / (1000 * 60 * 60 * 24 * 30))
  var days = Math.floor((distance / (1000 * 60 * 60 * 24)) - (months * 30));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  monthsSpan.innerHTML = months;
  daysSpan.innerHTML = days;
  hoursSpan.innerHTML = hours;
  minutesSpan.innerHTML = minutes;
  secondsSpan.innerHTML = seconds;

  // If the count down is finished, write some text 
//   if (distance < 0) {
//     clearInterval(x);
//     document.getElementById("demo").innerHTML = "EXPIRED";
//   }
}, 1000);